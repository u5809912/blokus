package comp1140.server;

import comp1140.server.Input.SubmittedPlayer;
import comp1140.server.gameEngine.Colour;
import comp1140.server.gameEngine.Game;

import java.net.URL;

/**
 * Created by steveb on 12/08/2015.
 */
public class BlokGame {

    /**
     * Parse a string representing a game state and determine whether it is legitimate.  The game may be in progress
     * (ie incomplete).
     * @param game A string representing the state of the game, as described in the assignment description.
     * @return True if the string represents a legitimate game state, according to the encoding defined in the
     * assignment description and the rules of the game.
     */
    public static Game legitimateGame(String game)
    {
        Game the_game = new Game ();

        the_game.SetupStandardTestingGame(game);
        the_game.RunGame();

        return the_game;
    }


    /**
     * Nick Sifniotis u5809912
     * 26/08/2015
     *
     * Full credit to Ben Roberts who provided the original version of this method.
     *
     * It fit in remarkable well with the code that I had written for the assignment itself.
     * Each of his TODOs translated to one method invocation - design win!
     *
     * Oh yeah
     * This method runs the tournament.
     * @return
     */
    public static int [] run_tournament(URL [] urls) throws ClassNotFoundException
    {
        SubmittedPlayer [] gamePlayers = new SubmittedPlayer[Colour.NUM_COLOURS];
        for (int i = 0; i < Colour.NUM_COLOURS; i++)
            gamePlayers[i] = new SubmittedPlayer(urls[i]);

        String gameState = "";
        Game game = null;
        int scores [] = new int[Colour.NUM_COLOURS];
        for (int i = 0; i < Colour.NUM_COLOURS; i++)
            scores[i] = -89;

        int nextPlayer = 0;
        do
        {
            String move = gamePlayers[nextPlayer].nextMove(gameState);

            if(move == null)
            {
                // end the game. disqualify the player whos turn it was
                if (game != null)
                    scores = game.ColoursScore();
                scores[nextPlayer] = -200;
                return scores;
            }

            gameState += " " + move;
            game = legitimateGame(gameState);
            scores = game.ColoursScore();

            if (!game.valid_game)
            {
                // end the game. disqualify the player whos turn it was
                scores[nextPlayer] = -200;
                return scores;
            }

            nextPlayer = (nextPlayer + 1) % 4;
        } while(!game.end_game);

        return scores;
    }


    public static void main(String[] args)
    {
       // if (args.length != 4)
        //{
      //      System.out.println ("FATAL ERROR: 4 ARGS EXPECTED, " + args.length + " RECEIVED");
     //       return;
     //   }

        URL [] urls = new URL[4];
        try {
            for (int i = 0; i < 4; i++)
                urls[i] = new URL(args[0]);
        }
        catch (Exception e)
        {
            System.out.println ("FATAL ERROR PARSING URL: " + e.getMessage());
            return;
        }

        int [] results;
        try {
            results = run_tournament(urls);
        }
        catch (ClassNotFoundException e)
        {
            System.out.println ("Exception! " + e.toString());
            return;
        }

        // print out the final scores, one on each line, until a better way is found to handle the score storage
        boolean disq = false;
        for (int i = 0; i < Colour.NUM_COLOURS; i ++)
            if (results[i] == -200)
                disq = true;

        if (disq)
        {
            for (int i = 0; i < Colour.NUM_COLOURS; i ++)
                System.out.println (results[i] == -200 ? "DISQ" : "NA");
        }
        else
        {
            for (int i = 0; i < Colour.NUM_COLOURS; i ++)
                System.out.println (results[i]);
        }

    }
}
