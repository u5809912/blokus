package comp1140.server.Input;

import comp1140.server.gameEngine.*;
import comp1140.server.gameEngine.Blocks.*;

import java.awt.*;
import java.util.List;

/**
 * Created by u5809912 on 15/08/15.
 *
 * Refactored by Nick Sif 24th August 2015
 * First so that it can properly handle multiple and or no spaces in the string
 * Second so that it conforms with the new MoveInputSource interface, that the game engine
 * will use to acquire every players next move without worrying about piddly details
 * like where the moves come from or how they are generated
 */
public class StringParser {

    private String input_source_string;


    /**
     * Nick Sifniotis u5809912
     * 24th August 2015
     *
     * Constructor for the StringParser object
     * This sucker will accept a stringified version of the game (As per the assignment specs)
     * and return placedblock objects corresponding to the moves that are stored within
     * that string.
     *
     * @param stringy_game - the representation of the game to be parsed
     */
    public StringParser(String stringy_game)
    {
        this.input_source_string = stringy_game;
    }



    /** Nick Sifniotis u5809912
     * 12:15pm Saturday 15th August 2015
     *
     * Accepts a four letter string code, as per the assignment specification, as well as the person who's current
     * turn it is, and returns the PlacedBlock object that the string translates to, or null if the colour skips a turn.
     *
     * Note that this function does not guarantee that the move that is made is valid! It is a translation of the
     * text it receives, nothing more.
     *
     * @param tokens - the string code to translate
     * @param colour - the colour whos turn it currently is
     * @return a playable PlacedBlock object
     */
    private PlacedBlock parse_token (char[] tokens, Colour colour)
    {
        int block_key = tokens[0] - 'A';
        int orientation = tokens[1] - 'A';
        int xcoord = tokens[2] - 'A';
        int ycoord = tokens[3] - 'A';

        // do some basic input checking. You never know what sort of errors might have snuck into the testing framework
        if ((xcoord < 0) || (xcoord > Board.BOARD_X))
            return new BadBlock();

        if ((ycoord < 0) || (ycoord > Board.BOARD_Y))
            return new BadBlock();

        if ((block_key < 0) || (block_key >= BlockInitialiser.NUM_BLOCKS))
            return new BadBlock();

        if ((orientation < 0) || (orientation >= 8))       // @TODO kill the magic number
            return new BadBlock();

        // start building the thing
        Rotations rotation = Rotations.NONE;
        Reflections reflection = ((orientation & 4) == 4) ? Reflections.VERTICAL : Reflections.NONE;

        switch (orientation % 4)
        {
            case 1:
                rotation = Rotations.QUARTER;
                break;
            case 2:
                rotation = Rotations.HALF;
                break;
            case 3:
                rotation = Rotations.THREEQUARTER;
                break;
        }

        PlacedBlock res = new PlacedBlock(BlockInitialiser.init_block(block_key), rotation, reflection, new Point(xcoord, ycoord), colour);
        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 24th August 2015
     *
     * The implementation of the MoveInputSource interface contract
     * The game engine cares not whence these moves come from
     *
     * @param board - the state of the board as it currently is
     * @param curr_colour - who's turn it is
     * @return - a placedBlock that corresponds to the move just made, or a BadBlock
     *           if the move made is invalid or garbage.
     */
    public PlacedBlock nextMove (Board board, Colour curr_colour, List<Block> unplayed_moves)
    {
        int position = 0;
        boolean skip_turn = false;
        char [] data = new char [4];            // I hate these magic numbers
        while (position < 4 && input_source_string.length() > 0 && !skip_turn)
        {
            data [position] = input_source_string.charAt(0);
            input_source_string = input_source_string.substring(1);
            if (data [position] == '.')
                skip_turn = true;

            if (data [position] >= 'A' && data[position] <= 'Z')
                position ++;
        }


        // return no move if the colour has chosen to skip their turn
        if (skip_turn)
            return new SkipTurnBlock();


        // return end of game if the parser found nothing but whitespace to the end of the string.
        if (position == 0)
            return new GameOverBlock();


        // the 'out of data' error. Position is only ever 4 when we have run out of characters
        // to process. This should never happen with the test data provided for this assignment.
        if (position < 4)
            return new BadBlock();


        // no sense in reinventing the wheel here. The existing method parse_token does the job
        // so I'll just refactor it to use the data that has been extracted from the string.
        return parse_token(data, curr_colour);
    }
}
