package comp1140.server.gameEngine;

import comp1140.server.gameEngine.Blocks.Block;

import java.awt.*;

/**
 * Created by u5809912 on 15/08/15.
 *
 * A temporary class that holds the data used to initialise the block shapes
 *
 * Will most likely be replaced by a data base or data file or some such in due course
 *
 */
public class BlockInitialiser {

    public static final int NUM_BLOCKS = 21;

    public static Block init_block (int blockId)
    {
        Block res;
        Point [] points = null;

        switch (blockId)
        {
            case 0:
                points = new Point [1];
                points[0] =  new Point(0, 0);
                break;
            case 1:
                points = new Point [2];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                break;
            case 2:
                points = new Point [3];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                break;
            case 3:
                points = new Point [3];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(1, 1);
                break;
            case 4:
                points = new Point [4];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(0, 3);
                break;
            case 5:
                points = new Point [4];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(-1, 2);
                break;
            case 6:
                points = new Point [4];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(1, 1);
                points[3] =  new Point(0, 2);
                break;
            case 7:
                points = new Point [4];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(1, 0);
                points[3] =  new Point(1, 1);
                break;
            case 8:
                points = new Point [4];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(1, 0);
                points[2] =  new Point(1, 1);
                points[3] =  new Point(2, 1);
                break;
            case 9:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(0, 3);
                points[4] =  new Point(0, 4);
                break;
            case 10:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(0, 3);
                points[4] =  new Point(-1, 3);
                break;
            case 11:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(-1, 2);
                points[4] =  new Point(-1, 3);
                break;
            case 12:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(-1, 1);
                points[4] =  new Point(-1, 2);
                break;
            case 13:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(1, 0);
                points[2] =  new Point(1, 1);
                points[3] =  new Point(1, 2);
                points[4] =  new Point(0, 2);
                break;
            case 14:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(0, 3);
                points[4] =  new Point(1, 1);
                break;
            case 15:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(1, 2);
                points[4] =  new Point(-1, 2);
                break;
            case 16:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(0, 2);
                points[3] =  new Point(1, 2);
                points[4] =  new Point(2, 2);
                break;
            case 17:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(1, 0);
                points[2] =  new Point(1, 1);
                points[3] =  new Point(2, 1);
                points[4] =  new Point(2, 2);
                break;
            case 18:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(1, 1);
                points[3] =  new Point(2, 1);
                points[4] =  new Point(2, 2);
                break;
            case 19:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(0, 1);
                points[2] =  new Point(1, 1);
                points[3] =  new Point(2, 1);
                points[4] =  new Point(1, 2);
                break;
            case 20:
                points = new Point [5];
                points[0] =  new Point(0, 0);
                points[1] =  new Point(-1, 1);
                points[2] =  new Point(0, 1);
                points[3] =  new Point(1, 1);
                points[4] =  new Point(0, 2);
                break;
        }

        res = new Block (blockId, points);
        return res;
    }
}
