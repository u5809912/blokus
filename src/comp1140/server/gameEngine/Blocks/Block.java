package comp1140.server.gameEngine.Blocks;

import java.awt.Point;
import java.util.*;

/**
 * Created by u5809912 on 15/08/15.
 */
public class Block {
    // @TODO these should not be public. Sort out proper access in due course.
    public Point [] squares;
    public Point [] badSquares;
    public Point [] connectingSquares;

    public int id;          // a kludgy solution to a kludgy problem
    public int score;       // more kludge. @TODO: eliminate the kludge

    public Point minimum;
    public Point maximum;

    /**
     * Default constructor. To make it compile
     */
    public Block ()
    {

    }

    /**
     * Nick Sifniotis u5809912
     * 6am Saturday August 15th, 2015
     *
     * Builds a block data structure based on the list of squares that make up this block
     * Compute which neighbouring squares another piece of the same colour can connect to,
     * and which neighbouring squares such a piece is not allowed to touch.
     *
     * @param squares - the list of squares that make up this block in it's default rotation
     *                (origin 0,0 at the top left corner)
     */
    public Block (int id, Point [] squares)
    {
        this.id = id;
        this.score = squares.length;

        // it makes sense, conceptually, to store point data as points
        // however, to process the 'bad squares' and 'connecting squares' lists quickly,
        // it makes more sense to encode the coordinates into a single integer, and then
        // convert them back into Points at the end of the processing.

        List <Integer> sq_coords = new ArrayList<>();
        List <Integer> bad_coords = new ArrayList <>();
        List <Integer> con_coords = new ArrayList<>();

        // $10 to anyone who can tell me why this works
        for (int i = 0; i < squares.length; i ++) {
            sq_coords.add ((squares[i].x + 10) * 100 + (squares[i].y + 10));
            for (int dx = -1; dx <= 1; dx ++)
                for (int dy = -1; dy <= 1; dy ++)
                    // you could call this testing the edge cases :v :v :o
                    if (Math.abs(dx + dy) % 2 == 0)
                        con_coords.add ((squares[i].x + dx + 10) * 100 + (squares[i].y + dy + 10));
                    else
                        bad_coords.add ((squares[i].x + dx + 10) * 100 + (squares[i].y + dy + 10));
        }

        Collections.sort(sq_coords);
        Collections.sort(bad_coords);
        Collections.sort(con_coords);

        // remove repeated elements
        sq_coords = uniquefy(sq_coords);
        bad_coords = uniquefy(bad_coords);
        con_coords = uniquefy(con_coords);

        // remove any instances of squares from bad_squares and con_squares
        bad_coords = difference(bad_coords, sq_coords);
        con_coords = difference(con_coords, sq_coords);

        // remove all bad squares from con squares
        con_coords = difference(con_coords, bad_coords);

        // these three lists now contain the data that the system needs to interact with these blocks
        this.squares = convert_to_array(sq_coords);
        this.badSquares = convert_to_array(bad_coords);
        this.connectingSquares = convert_to_array(con_coords);

        recalculate_bounds();
    }

    /**
     * Nick Sifniotis u5809912
     * 6:35am Saturday 15th August 2015
     *
     * Returns a string representation of this block
     * Squares are represented by O, bad squares by . and connecting squares by ^
     * The origin is represented by 0 (for rotation testing)
     *
     * @return a string
     */
    @Override
    public String toString ()
    {
        int max_x = maximum.x + 2;
        int max_y = maximum.y + 2;
        int min_x = minimum.x - 2;
        int min_y = minimum.y - 2;

        int [] [] data = new int [max_x - min_x][max_y - min_y];

        for (Point p: connectingSquares)
            data [p.x - min_x][p.y - min_y] = 3;

        for (Point p: badSquares)
            data [p.x - min_x][p.y - min_y] = 2;

        for (Point p: squares)
            data [p.x - min_x][p.y - min_y] = 1;

        String res = "";

        for (int j = 0; j < max_y - min_y; j ++)
        {
            for (int i = 0; i < max_x - min_x; i ++)
                switch (data[i][j]) {
                    case 0:
                        res += " ";
                        break;
                    case 1:
                        res += "O";
                        break;
                    case 2:
                        res += ".";
                        break;
                    case 3:
                        res += "^";
                        break;
                }
            res += "\n";
        }

        return res;
    }

    /**
     * Nick Sifniotis u5809912
     * 6:30am Saturday 15th August 2015
     *
     * A helper function that will return an array of points from a list of single-int coordinates
     * @param data - the list to convert
     * @return - the resultant array
     */
    private Point [] convert_to_array (List <Integer> data)
    {
        Point [] res = new Point [data.size()];
        for (int i = 0, j = data.size(); i < j; i ++)
            res[i] = new Point ((data.get(i) / 100) - 10, (data.get(i) % 100) - 10);

        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 6:15am Saturday August 15th 2015
     *
     * Removes any elements within target that appear in exclusions
     * Assumes the two lists are sorted. If they are not, this function will not work.
     *
     * @param target
     * @param exclusions
     * @return a list of elements in target that do not appear in exclusions
     */
    private List<Integer> difference (List<Integer> target, List<Integer> exclusions)
    {
        List <Integer> res = new ArrayList<>();

        Iterator<Integer> t_it = target.iterator();
        Iterator<Integer> e_it = exclusions.iterator();
        int curr_t;
        int curr_e = e_it.next();

        while (t_it.hasNext())
        {
            curr_t = t_it.next();
            while (e_it.hasNext() && curr_e < curr_t)
                curr_e = e_it.next();

            if (curr_e != curr_t)
                res.add(curr_t);
        }

        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 6:10am Saturday August 15th 2015
     *
     * This function accepts a sorted list of integers and returns that same list with duplicate elements removed
     *
     * @param list - the list to uniquify
     * @return - the uniquified list
     */
    private List<Integer> uniquefy (List<Integer> list)
    {
        if (list.size() < 2)
            return list;

        List<Integer> res = new ArrayList<>();
        int last = list.get(0);
        res.add (last);

        for (int i = 1, j = list.size(); i < j; i ++)
            if (list.get(i) != last)
            {
                last = list.get(i);
                res.add (last);
            }
        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 12:50pm Tuesday 18th August 2015
     *
     * Compute the boundary of this block based on the location of the block's sqaures
     * as contained in the squares[] array
     */
    public void recalculate_bounds()
    {
        // find the bounding box that contains the squares
        minimum = new Point (squares[0].x, squares[0].y);
        maximum = new Point (squares[0].x, squares[0].y);

        for (Point p: squares)
        {
            if (maximum.x < p.x)
                maximum.x = p.x;
            if (minimum.x > p.x)
                minimum.x = p.x;

            if (maximum.y < p.y)
                maximum.y = p.y;
            if (minimum.y > p.y)
                minimum.y = p.y;
        }
    }
}
