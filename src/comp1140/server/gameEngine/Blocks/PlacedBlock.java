package comp1140.server.gameEngine.Blocks;

import comp1140.server.gameEngine.*;

import java.awt.*;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by u5809912 on 15/08/15.
 */
public class PlacedBlock extends Block {
    // @TODO: This should not be public
    public Colour colour;

    public Square[] square_masks;

    // we should not be storing this data damn it
    // but we need to be able to reverse a placedblock back into that
    // standard string representation, because the testing regime says
    // that it must be so.
    //
    // not happy
    private Reflections reflection;
    private Rotations rotation;
    private Point origin;

    /**
     * Nick Sifniotis u5809912
     * 11:30am Saturday 15th August 2015
     *
     * Null constructor
     * Because I want my code to compile.
     */
    public PlacedBlock () {
    }


    /**
     *
     * Nick Sifniotis u5809912
     * 7:00am Saturday 15th August 2015
     *
     * Returns a new PlacedBlock object
     *
     * @param parent - the original block shape that is being placed
     * @param rotation - which rotation to apply
     * @param reflection - which reflection to apply
     * @param position - where on the board this block is being placed
     * @param colour - who is placing the block
     */
    public PlacedBlock (Block parent, Rotations rotation, Reflections reflection, Point position, Colour colour)
    {
        this.colour = colour;
        this.origin = position;
        this.rotation = rotation;
        this.reflection = reflection;

        // deep copy the parent's data, before manipulating it
        this.id = parent.id;
        this.score = parent.score;
        this.squares = deep_copy(parent.squares);
        this.badSquares = deep_copy(parent.badSquares);
        this.connectingSquares = deep_copy(parent.connectingSquares);
        this.minimum = new Point (parent.minimum.x, parent.minimum.y);
        this.maximum = new Point (parent.maximum.x, parent.maximum.y);

        // apply the three transformations to the block - reflection, rotation, translation
        apply_reflection(reflection);
        apply_rotation(rotation);
        apply_translation(position);
        nix_outtabounds();

        // create the mask array
        List<Square> mega_list = new ArrayList<>();

        for (Point p: squares)
            mega_list.add (new Square (p.x, p.y, colour.placement_bitmask(), true));
        for (Point p: badSquares)
            mega_list.add (new Square (p.x, p.y, colour.bad_square_bitmask(), false));
        for (Point p: connectingSquares)
            mega_list.add (new Square (p.x, p.y, colour.connecting_square_bitmask(), false));

        square_masks = mega_list.toArray(new Square [mega_list.size()]);

        recalculate_bounds();
    }


    /**
     * Nick Sifniotis u5809912
     * 7:00am Saturday 15th August 2015
     *
     * Utility function to make a deep copy of parent's point data.
     *
     * @param data - the data to copy
     * @return - the copied data
     */
    private Point [] deep_copy (Point [] data)
    {
        Point [] res = new Point [data.length];
        for (int i = 0, j = data.length; i < j; i ++)
            res[i] = new Point(data[i]);

        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 7:25am Saturday 15th August 2015
     *
     * Applies a translation to this block's coordinates
     *
     * @param point - the point to translate to
     */
    public void apply_translation (Point point)
    {
        for (Point p: squares) {
            p.x += point.x;
            p.y += point.y;
        }

        for (Point p: badSquares) {
            p.x += point.x;
            p.y += point.y;
        }

        for (Point p: connectingSquares) {
            p.x += point.x;
            p.y += point.y;
        }
    }

    /**
     * Nick Sifniotis u5809912
     * 7:10am Saturday 15th August 2015
     *
     * Applies rotations to this block.
     *
     * There is probably a smarter way to solve this problem.
     *
     * @param rotation
     */
    private void apply_rotation (Rotations rotation)
    {
        switch (rotation)
        {
            case HALF:
                for (Point p: squares)
                {
                    p.x = -p.x;
                    p.y = -p.y;
                }
                for (Point p: badSquares)
                {
                    p.x = -p.x;
                    p.y = -p.y;
                }
                for (Point p: connectingSquares)
                {
                    p.x = -p.x;
                    p.y = -p.y;
                }
                break;

            case THREEQUARTER:
                for (Point p: squares)
                {
                    int tx = p.x;
                    int ty = p.y;

                    p.x = ty;
                    p.y = -tx;
                }
                for (Point p: badSquares)
                {
                    int tx = p.x;
                    int ty = p.y;

                    p.x = ty;
                    p.y = -tx;
                }
                for (Point p: connectingSquares)
                {
                    int tx = p.x;
                    int ty = p.y;

                    p.x = ty;
                    p.y = -tx;
                }
                break;

            case QUARTER:
                for (Point p: squares)
                {
                    int tx = p.x;
                    int ty = p.y;

                    p.x = -ty;
                    p.y = tx;
                }
                for (Point p: badSquares)
                {
                    int tx = p.x;
                    int ty = p.y;

                    p.x = -ty;
                    p.y = tx;
                }
                for (Point p: connectingSquares)
                {
                    int tx = p.x;
                    int ty = p.y;

                    p.x = -ty;
                    p.y = tx;
                }
                break;
        }
    }

    /**
     * Nick Sifniotis u5809912
     * 7:05am Saturday 15th August 2015
     *
     * Applies reflections to this block
     *
     * @param reflection
     */
    private void apply_reflection (Reflections reflection)
    {
        switch (reflection)
        {
            case VERTICAL:
                for (Point p: squares)
                    p.x = -p.x;

                for (Point p: badSquares)
                    p.x = -p.x;

                for (Point p: connectingSquares)
                    p.x = -p.x;

                break;
        }
    }


    /** Nick Sifniotis u5809912
     * 11:55am Saturday 15th August 2015
     *
     * This function goes through badSquares and connectorSquares
     * and removes from those lists any points that lie outside the game board boundary.
     *
     * This is because a piece may legally be played with such points existant. Only squares
     * themselves are not allowed to live outside the game board. However, even though these points
     * may exist, they don't, and they'll cause array out of bounds errors. So fuck them.
     */
    private void nix_outtabounds ()
    {
        List<Point> newBad;

        newBad = new ArrayList<>();
        for (Point p: badSquares)
            if (!((p.x < 0) || (p.x >= Board.BOARD_X) || (p.y < 0) || (p.y >= Board.BOARD_Y)))
                newBad.add(p);
        badSquares = new Point[newBad.size()];
        newBad.toArray(badSquares);

        newBad = new ArrayList<>();
        for (Point p: connectingSquares)
            if (!((p.x < 0) || (p.x >= Board.BOARD_X) || (p.y < 0) || (p.y >= Board.BOARD_Y)))
                newBad.add(p);
        connectingSquares = new Point[newBad.size()];
        newBad.toArray(connectingSquares);
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Returns the string representation of this placed block / move
     *
     * @return
     */
    public String Stringify()
    {
        char block_code = (char)('A' + this.id);
        char reflect_code = (char)('A' + (reflection.ordinal() * 4 + rotation.ordinal()));
        char x_code = (char)('A' + origin.x);
        char y_code = (char)('A' + origin.y);
        char [] array = { block_code, reflect_code, x_code, y_code };
        return new String (array);
    }
}
