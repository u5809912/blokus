package comp1140.server.gameEngine;

import comp1140.server.gameEngine.Blocks.PlacedBlock;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by u5809912 on 15/08/15.
 */
public class Board {

    public static final int BOARD_X = 20;
    public static final int BOARD_Y = 20;

    // board_status is a crafty bitmap
    // the lowest four bits represent squares that a colour cannot be placed into
    // the next four bits represent 'connecting squares' that blocks being placed must be placed into
    // the next four represent which colour/colour an already placed block (when the lowest four bits are all high)
    // belongs to. Strictly speaking it is only the AI and the interface that needs to know who owns the sqaure.
    private short [] [] board_status;


    /**
     * Nick Sifniotis u5809912
     * 7.43am Saturday 15th August 2015
     * Modified 24th August 2015
     *
     * Construct the game board
     * and initialise the four corner spaces where a valid move may be played
     */
    public Board ()
    {
        board_status = new short [BOARD_X][BOARD_Y];

        short first_move_connector = (short)(Colour.BLUE.connecting_square_bitmask() |
                                                Colour.YELLOW.connecting_square_bitmask() |
                                                Colour.RED.connecting_square_bitmask() |
                                                Colour.GREEN.connecting_square_bitmask());

        board_status[0][0] = first_move_connector;
        board_status[BOARD_X - 1][0] = first_move_connector;
        board_status[0][BOARD_Y - 1] = first_move_connector;
        board_status[BOARD_X - 1][BOARD_Y - 1] = first_move_connector;
    }


    /**
     * Nick Sifniotis u58099812
     * 6:48am Saturday 15th August 2015
     *
     * Plays the block onto the board.
     *
     * @param block - the block that the colour is trying to play
     */
    public void playBlock (PlacedBlock block)
    {
        if (!isValidMove(block))
            return;

        for (Square s: block.square_masks)
            board_status[s.x][s.y] |= s.mask;
    }


    /**
     * Nick Sifniotis u5809912
     * 7:41am Saturday 15th August 2015
     *
     * Determines whether the block that we are trying to place (it has come pre-rotated and pre-translated for us)
     * can in fact be placed without violating the game rules.
     *
     * Return true - we trust the humans not to cheat.
     *
     * Updated - we no longer simply trust the humans, we verify the validitty of the move
     *
     * @param block
     * @return
     */
    public boolean isValidMove (PlacedBlock block)
    {
        // there are three challenges that the block must face before it can be accepted as a valid move
        // the first - is it trying to place itself over the board limits?
        // the second - is it trying to place itself onto a square marked as bad or occupied?
        // the third - does it touch at least one corner of the same colour?

        boolean first_gate = (block.minimum.x < 0) | (block.minimum.y < 0) | (block.maximum.x >= BOARD_X) | (block.maximum.y >= BOARD_Y);
        if (first_gate)
            return false;


        boolean second_gate = false;
        boolean third_gate = false;

        //  if the first gate is not passed, this code would generate array out of bounds errors
        for (Square s: block.square_masks) {
            if (s.placable) {
                second_gate |= (board_status[s.x][s.y] & block.colour.bad_square_bitmask()) > 0;
                third_gate |= (board_status[s.x][s.y] & block.colour.connecting_square_bitmask()) > 0;
            }
        }
        if (second_gate)
            return false;

        return third_gate;
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Returns a list of Points that contain the coordinates of every square on the board
     * which currently identifies as a valid connecting square.
     * @param c
     * @return
     */
    public List<Point> GetConnectingSquares (Colour c)
    {
        List <Point> res = new ArrayList<>();

        for (int i = 0; i < BOARD_X; i ++)
            for (int j = 0; j < BOARD_Y; j ++)
                if ((board_status[i][j] & c.connecting_square_bitmask()) > 0)
                    if ((board_status[i][j] & c.bad_square_bitmask()) == 0)
                        res.add (new Point (i, j));

        return res;
    }
}
