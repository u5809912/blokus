package comp1140.server.gameEngine;

/**
 * Created by u5809912 on 15/08/15.
 */
public enum Colour {
    BLUE, YELLOW, RED, GREEN;

    public static final int NUM_COLOURS = Colour.values().length;


    /**
     * Nick Sifniotis u5809912
     * 15th August 2015
     *
     * Given the current colour, returns the next one.
     *
     * @return the next colour to have a go
     */
    public Colour next() {
        Colour res = BLUE;
        switch (this) {
            case BLUE:
                res = YELLOW;
                break;
            case YELLOW:
                res = RED;
                break;
            case RED:
                res = GREEN;
                break;
            case GREEN:
                res = BLUE;         // redundant, but done for the sake of completeness
        }

        return res;
    }

    /**
     * Nick Sifniotis u5809912
     * 10:30am Tuesday 18th August 2015
     *
     * This function returns the bitmask to be applied to a square when a block is being placed on the board
     * The four highest bits identify which colour block is being placed, the middle four are 'connection square'
     * indicators and irrelevant here, and the low bits remain 'bad squares' but for every coloured block.
     *
     * The only reason data is being saved in the high bits at all is so that the output streams and the artificial
     * intelligence agents know which colours have been placed in each square. That could be important information.
     *
     * @return a short that can be ORed directly onto the board (via a placedblock object)
     */
    public short placement_bitmask()
    {
        short res = 0;

        switch (this)
        {
            case BLUE:
                res = 0b0000000100001111;
                break;
            case RED:
                res = 0b0000001000001111;
                break;
            case YELLOW:
                res = 0b0000010000001111;
                break;
            case GREEN:
                res = 0b0000100000001111;
                break;
        }

        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 10:35am Tuesday 18th August 2015
     *
     * Returns a bitmask to be ORed onto the game board for a square that has been identified as a
     * 'bad square' - one in which a block of the same colour cannot be placed.
     *
     * @return a sixteen bit bitmask to be ORed against the board (via a placedBlock)
     */
    public short bad_square_bitmask()
    {
        short res = 0;

        switch (this)
        {
            case BLUE:
                res = 0b0000000000000001;
                break;
            case YELLOW:
                res = 0b0000000000000010;
                break;
            case RED:
                res = 0b0000000000000100;
                break;
            case GREEN:
                res = 0b0000000000001000;
                break;
        }

        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 10:39am Tuesday 18th August 2015
     *
     * Returns a bitmask to be ORed onto the game board for a square that has been identified as a
     * 'connecting square' - one in which at least one square of a block of the same colour must be placed.
     *
     * @return a sixteen bit bitmask to be ORed against the board (via a placedBlock)
     */
    public short connecting_square_bitmask()
    {
        short res = 0;

        switch (this)
        {
            case BLUE:
                res = 0b0000000000010000;
                break;
            case YELLOW:
                res = 0b0000000000100000;
                break;
            case RED:
                res = 0b0000000001000000;
                break;
            case GREEN:
                res = 0b0000000010000000;
                break;
        }

        return res;
    }
}