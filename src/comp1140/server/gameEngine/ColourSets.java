package comp1140.server.gameEngine;

import comp1140.server.gameEngine.Blocks.Block;
import comp1140.server.gameEngine.Blocks.PlacedBlock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nsifniotis on 25/08/15.
 *
 * The way this game is designed, one player is able to control more than one colour set, and
 * one colour set is able to be controlled by more than one player (!)
 *
 * This means that the coloursets need to be kept in a structure that is separate from the players,
 * and some sort of mapping needs to be designed to link the two together.
 *
 * This class contains the four coloursets.
 */
public class ColourSets {

    private List<Block>[] block_sets;


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * The constructor.
     */
    public ColourSets ()
    {
        reset ();
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Re-initialised the four bock sets. Used when the object is constructed,
     * and when a player chucks a tanty and restarts the game.
     */
    public void reset()
    {
        this.block_sets = new ArrayList[Colour.NUM_COLOURS];

        for (int i = 0; i < Colour.NUM_COLOURS; i ++)
            this.block_sets[i] = new ArrayList<>();

        Block temp_block;
        for (int i = 0; i < BlockInitialiser.NUM_BLOCKS; i ++)
        {
            temp_block = BlockInitialiser.init_block(i);
            for (int j = 0; j < Colour.NUM_COLOURS; j ++)
                this.block_sets[j].add(temp_block);
        }
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Accessor functions for the sets of coloured blocks that this data structure holds.
     *
     * @param index, colour - the particular list that needs to be returned
     * @return - the list of blocks
     */
    public List<Block> GetList(int index)
    {
        return block_sets[index];
    }

    public List<Block> GetList (Colour colour)
    {
        return GetList (colour.ordinal());
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Once a block has been played, it needs to be removed from the list of blocks that have not yet been played.
     * Obviously ...
     *
     * Returns true if there are more blocks remaining, or false otherwise
     *
     * @param block - the block that has just been placed onto the board.
     */
    public boolean RemovePlayed(PlacedBlock block)
    {
        int c = block.colour.ordinal();
        int id = block.id;
        Block target = null;

        for (Block b: block_sets[c])
            if (b.id == id)
                target = b;

        if (target != null)
            block_sets[c].remove(target);

        return (block_sets[c].size() != 0);
    }
}
