package comp1140.server.gameEngine;

import comp1140.server.Input.StringParser;
import comp1140.server.gameEngine.Blocks.*;

/**
 * Created by u5809912 on 15/08/15.
 */
public class Game {

    private Board game_board;
    private Players game_players;
    private int [] scores_by_colour;

    private Colour curr_colour;

    public boolean valid_game;
    public boolean end_game;

    public Game ()
    {
        game_board = new Board();
        game_players = new Players();
        scores_by_colour = new int [Colour.NUM_COLOURS];

        for (int i = 0; i < Colour.NUM_COLOURS; i ++)
            scores_by_colour [i] = -89;
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * A 'standard testing game' is one where the player inputs are piped in through the one
     * stringbuilder object. There may as well be just the one player since all the information
     * is coming in through the one moveinputsource anyway.
     *
     */
    public void SetupStandardTestingGame (String game_source)
    {
        StringParser input_device = new StringParser(game_source);

        game_players.AddPlayer(input_device, "String-Fed Player");

        game_players.AssignColourToPlayer(0, Colour.BLUE);
        game_players.AssignColourToPlayer(0, Colour.YELLOW);
        game_players.AssignColourToPlayer(0, Colour.RED);
        game_players.AssignColourToPlayer(0, Colour.GREEN);

        valid_game = true;
        end_game = false;
    }


    public void RunGame ()
    {
        curr_colour = Colour.BLUE;
        PlacedBlock move;
        int skips_in_a_row = 0;

        while (skips_in_a_row < 4 && valid_game && !end_game)
        {
            move = this.NextMove();

            // is it a valid move or some other thing?
            if (move instanceof BadBlock)
            {
                valid_game = false;
            }
            else if (move instanceof GameOverBlock)
            {
                // believe it or not thats a misnomer. It should be 'end of input block' or somesuch
                // just break out until a more elegant solution presents itself
                return;
            }
            else if (move instanceof SkipTurnBlock)
            {
                skips_in_a_row++;
                curr_colour = curr_colour.next();
            }
            else
            {
                // it's a move. Make sure it is legit.
                if (game_board.isValidMove(move))
                {
                    skips_in_a_row = 0;
                    game_board.playBlock(move);
                    boolean pieces_remaining = game_players.RemovePlayed(move);
                    scores_by_colour[curr_colour.ordinal()] += move.score;
                    if (!pieces_remaining)
                    {
                        scores_by_colour[curr_colour.ordinal()] += 15;
                        if (move.id == 0)
                            scores_by_colour[curr_colour.ordinal()] += 5;
                    }
                    curr_colour = curr_colour.next();
                }
                else
                {
                    valid_game = false;
                }
            }
        }

        if (skips_in_a_row == 4)
            end_game = true;
    }


    /** Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Returns the next move from the player currently in control of curr_colour
     *
     * @return
     */
    public PlacedBlock NextMove ()
    {
        // get the player who is in charge of these colours.
        Player curr_player = game_players.WhosTurnIsIt(curr_colour);

        // get the next move from the player
        return curr_player.get_next_move(game_board, curr_colour);
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Two functions for obtaining the current score of a game.
     * The first returns an array of 4 ints - these are the scores from the point of view of the colour sets
     * The second returns an array of n ints, where n is the number of players
     * Players who are not controlling any colours (in this game, in other words) receive a score of -100
     * which places them dead last. Yay magic numbers
     *
     * @return
     */
    public int [] ColoursScore ()
    {
        return scores_by_colour;
    }

}
