package comp1140.server.gameEngine;

import comp1140.server.Input.StringParser;
import comp1140.server.gameEngine.Blocks.PlacedBlock;
import comp1140.server.gameEngine.Blocks.SkipTurnBlock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nsifniotis on 25/08/15.
 */
public class Player {

    private List<Colour> colours;
    private StringParser input_source;
    private String name;
    private ColourSets unplayed_blocks;


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * A set of constructors with for which to create player objects.
     * Its not known right now which constructors will and will not be needed.
     *
     * @param input_source - a link to the input device or AI that will generate the moves
     * @param name - the player's name
     */
    public Player (StringParser input_source, String name, ColourSets unplayed_blocks)
    {
        this(name, unplayed_blocks);
        setInputSource (input_source);
    }

    public Player (String name, ColourSets unplayed_blocks)
    {
        this (unplayed_blocks);
        setPlayerName(name);
    }

    public Player (ColourSets unplayed_blocks)
    {
        colours = new ArrayList<>();
        this.unplayed_blocks = unplayed_blocks;
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Adds or removes the colour C to the list of colours controlled by this player.
     *
     * @param c
     */
    public void AddColour (Colour c)
    {
        if (!colours.contains(c))
            colours.add (c);
    }

    public void RemoveColour (Colour c)
    {
        colours.remove(c);
    }

    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Setter functions for protected properties of the Player class
     * Name and InputSource can be selected by the human as the game is being set up
     * so it must be possible to change these before the game starts
     *
     */
    public void setPlayerName (String name)
    {
        this.name = name;
    }

    public void setInputSource (StringParser input_source)
    {
        this.input_source = input_source;
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * The player is passed a colour and the current state of the board
     * and it is expected to return a move.
     *
     * @param game_board
     * @param colour
     * @return
     */
    public PlacedBlock get_next_move (Board game_board, Colour colour)
    {
        if (!colours.contains(colour))
            return new SkipTurnBlock();

        return input_source.nextMove(game_board, colour, this.unplayed_blocks.GetList(colour));
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Accessor or 'get' function for the virtual private boolean readyToPlay
     * Which represents whether the player object has been initialised and is ready to rumble
     * @return
     */
    public boolean ReadyToPlay ()
    {
        boolean res = true;

        res &= !(this.name.equals(""));
        res &= !(this.input_source == null);
        res &= !(this.unplayed_blocks == null);

        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Returns a string signalling the current status of this player.
     *
     * @return
     */
    @Override
    public String toString ()
    {
        String res = (this.name.equals ("") ? "<UNNAMED PLAYER>" : this.name);
        if (this.ReadyToPlay())
        {
            res += " reports ready to play and is responsible for ";

            String cols = "";
            for (Colour c: colours)
                cols += " " + c.toString();

            res += (cols.equals ("") ? "no colours." : "colours" + cols);
        }
        else
        {
            res += " reports NOT READY to play yet.";
        }

        return res;
    }

}
