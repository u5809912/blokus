package comp1140.server.gameEngine;

import comp1140.server.Input.StringParser;
import comp1140.server.gameEngine.Blocks.PlacedBlock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nsifniotis on 25/08/15.
 */
public class Players {

    private List<Player> players;
    private List<Player> [] player_map;
    private int [] player_counter;
    private ColourSets unplayed_blocks;

    public Players()
    {
        unplayed_blocks = new ColourSets();

        players = new ArrayList<>();
        player_map = new ArrayList[Colour.NUM_COLOURS];
        player_counter = new int[Colour.NUM_COLOURS];

        for (int i = 0; i < Colour.NUM_COLOURS; i ++)
        {
            player_map[i] = new ArrayList<>();
            player_counter[i]  = -1;
        }
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Creates a new player and adds them to the stable.
     * 4 different such methods created for maximum flexibility in game design
     *
     * @param input_source
     * @param name
     */
    public void AddPlayer (StringParser input_source, String name)
    {
        this.players.add(new Player (input_source, name, unplayed_blocks));
    }

    public void AddPlayer (StringParser input_source)
    {
        this.AddPlayer(input_source, "Player " + players.size());
    }

    public void AddPlayer (String name)
    {
        this.players.add(new Player(name, unplayed_blocks));
    }

    public void AddPlayer ()
    {
        this.AddPlayer("Player " + players.size());
    }


    public void AssignColourToPlayer (int player, Colour c)
    {
        Player p;
        try {
            p = players.get(player);
        }
        catch (Exception e)
        {
            return;
        }
        if (p == null)
            return;

        if (!player_map[c.ordinal()].contains(p))
        {
            player_map[c.ordinal()].add (p);
            p.AddColour (c);
            player_counter[c.ordinal()] = 0;
        }
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * Given the colour c, returns the player whos turn it is to control this colour
     * Most times it will be the same player over and over
     * But sometimes, multiple agents will be able to control the one colour
     * and take turns placing those blocks.
     *
     * player_counter keeps track of which player will be taking the next turn. This counter is
     * incremented and modulod by the number of players looking after that colour
     * Most of the time it will be (0 + 1) mod 1 == 0
     *
     * @param c - the colour that we're interested in
     * @return - the player taking the next move for this colour
     */
    public Player WhosTurnIsIt (Colour c)
    {
        int colour_id = c.ordinal();
        Player res = player_map [colour_id].get(player_counter[colour_id]);
        player_counter[colour_id] = ((player_counter[colour_id] + 1) % player_map[colour_id].size());
        return res;
    }


    /**
     * Nick Sifniotis u5809912
     * 25/08/2015
     *
     * It's been a very productive day!
     *
     * Returns TRUE if there are more unplayed blocks left in this list, or FALSE otherwise
     *
     * @param b - the block that has just been played
     */
    public boolean RemovePlayed(PlacedBlock b)
    {
        return unplayed_blocks.RemovePlayed(b);
    }

}
