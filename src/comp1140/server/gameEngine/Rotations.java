package comp1140.server.gameEngine;

/**
 * Created by u5809912 on 15/08/15.
 */
public enum Rotations {
    NONE, QUARTER, HALF, THREEQUARTER
}
