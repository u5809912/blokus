package comp1140.server.gameEngine;

/**
 * Created by u5809912 on 18/08/15.
 *
 * Contains all the data pertaining to a Square that has been occupied by a Placed Block
 *
 *
 */
public class Square extends java.awt.Point {

    public short mask;
    public boolean placable;

    public Square (int x, int y, short mask, boolean placable)
    {
        this.x = x;
        this.y = y;
        this.mask = mask;
        this.placable = placable;
    }
}
